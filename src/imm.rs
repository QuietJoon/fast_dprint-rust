// =============================================================================
// Public Macros for "imm" Mode Logging
// =============================================================================

///
/// 2. `f_conditional_log!` chooses between two logging functions based on a cfg flag.
///    Under the condition (e.g. debug_assertions), it calls a buffer-based macro (with error handling);
///    otherwise, it calls a printing macro (which returns ()):
///
#[doc(hidden)]
#[macro_export]
macro_rules! f_conditional_log {
    ($cfg:meta, $buf:expr, $write_macro:ident, $print_macro:ident, $fmt:expr $(, $($args:expr),*)?) => {{
        #[cfg(feature = "imm")]
        {
            if let Err(e) = $write_macro!($buf, $fmt $(, $($args),*)?) {
                eprintln!(concat!(stringify!($write_macro), " write_macro_error: {}"), e);
            }
        }
        #[cfg(not(feature = "imm"))]
        {
            $print_macro!($fmt $(, $($args),*)?);
        }
    }};
}

///
/// 3. `f_conditional_log_rev!` is like `f_conditional_log!` but swaps the branches:
///    under the negated condition it uses the buffering method, and under the condition it uses printing.
///    (Useful for “use buffer in release” variants.)
///
#[doc(hidden)]
#[macro_export]
macro_rules! f_conditional_log_rev {
    ($cfg:meta, $buf:expr, $write_macro:ident, $print_macro:ident, $fmt:expr $(, $($args:expr),*)?) => {{
        #[cfg(not(feature = "imm"))]
        {
            if let Err(e) = $write_macro!($buf, $fmt $(, $($args),*)?) {
                eprintln!(concat!(stringify!($write_macro), " write_macro_error: {}"), e);
            }
        }
        #[cfg(feature = "imm")]
        {
            $print_macro!($fmt $(, $($args),*)?);
        }
    }};
}

///
/// 4. `f_conditional_print!` calls a printing macro only when a cfg condition holds.
///
#[doc(hidden)]
#[macro_export]
macro_rules! f_conditional_print {
    ($cfg:meta, $print_macro:ident, $fmt:expr $(, $($args:expr),*)?) => {{
        #[cfg(feature = "imm")]
        {
            $print_macro!($fmt $(, $($args),*)?);
        }
    }};
}

// === Use Buffer When in "imm" Mode; Otherwise, Print ===

#[macro_export]
macro_rules! bip {
    ($buf:expr, $fmt:expr $(, $($args:expr),*)?) => {{
        f_conditional_log!(imm, $buf, write, print, $fmt $(, $($args),*)?)
    }};
}

#[macro_export]
macro_rules! bipl {
    ($buf:expr, $fmt:expr $(, $($args:expr),*)?) => {{
        f_conditional_log!(imm, $buf, writeln, println, $fmt $(, $($args),*)?)
    }};
}

#[macro_export]
macro_rules! biep {
    ($buf:expr, $fmt:expr $(, $($args:expr),*)?) => {{
        f_conditional_log!(imm, $buf, write, eprint, $fmt $(, $($args),*)?)
    }};
}

#[macro_export]
macro_rules! biepl {
    ($buf:expr, $fmt:expr $(, $($args:expr),*)?) => {{
        f_conditional_log!(imm, $buf, writeln, eprintln, $fmt $(, $($args),*)?)
    }};
}

// === Use Buffer in Non-"imm" Mode; Otherwise, Print ===

#[macro_export]
macro_rules! bnp {
    ($buf:expr, $fmt:expr $(, $($args:expr),*)?) => {{
        f_conditional_log_rev!(imm, $buf, write, print, $fmt $(, $($args),*)?)
    }};
}

#[macro_export]
macro_rules! bnpl {
    ($buf:expr, $fmt:expr $(, $($args:expr),*)?) => {{
        f_conditional_log_rev!(imm, $buf, writeln, println, $fmt $(, $($args),*)?)
    }};
}

#[macro_export]
macro_rules! bnep {
    ($buf:expr, $fmt:expr $(, $($args:expr),*)?) => {{
        f_conditional_log_rev!(imm, $buf, write, eprint, $fmt $(, $($args),*)?)
    }};
}

#[macro_export]
macro_rules! bnepl {
    ($buf:expr, $fmt:expr $(, $($args:expr),*)?) => {{
        f_conditional_log_rev!(imm, $buf, writeln, eprintln, $fmt $(, $($args),*)?)
    }};
}

// === Use Buffer in "imm" Mode Only; No Output in Non-"imm" Mode ===

#[macro_export]
macro_rules! biop {
    ($buf:expr, $fmt:expr $(, $($args:expr),*)?) => {{
        #[cfg(feature = "imm")]
        {
            log_buffer!(write, $buf, $fmt $(, $($args),*)?)
        }
    }};
}

#[macro_export]
macro_rules! biopl {
    ($buf:expr, $fmt:expr $(, $($args:expr),*)?) => {{
        #[cfg(feature = "imm")]
        {
            log_buffer!(writeln, $buf, $fmt $(, $($args),*)?)
        }
    }};
}

/* Same as `biop`, `biopl`
#[macro_export]
macro_rules! bioep {
}

#[macro_export]
macro_rules! bioepl {
}
*/

// === Print Only in "imm" Mode (No Buffer) ===

#[macro_export]
macro_rules! ip {
    ($fmt:expr $(, $($args:expr),*)?) => {{
        f_conditional_print!(imm, print, $fmt $(, $($args),*)?)
    }};
}

#[macro_export]
macro_rules! ipl {
    ($fmt:expr $(, $($args:expr),*)?) => {{
        f_conditional_print!(imm, println, $fmt $(, $($args),*)?)
    }};
}

#[macro_export]
macro_rules! iep {
    ($fmt:expr $(, $($args:expr),*)?) => {{
        f_conditional_print!(imm, eprint, $fmt $(, $($args),*)?)
    }};
}

#[macro_export]
macro_rules! iepl {
    ($fmt:expr $(, $($args:expr),*)?) => {{
        f_conditional_print!(imm, eprintln, $fmt $(, $($args),*)?)
    }};
}

// === Use Buffer in Non-"imm" Mode Only; No Output in "imm" Mode ===

#[macro_export]
macro_rules! bnop {
    ($buf:expr, $fmt:expr $(, $($args:expr),*)?) => {{
        #[cfg(feature = "imm")]
        {
            log_buffer!(write, $buf, $fmt $(, $($args),*)?)
        }
    }};
}

#[macro_export]
macro_rules! bnopl {
    ($buf:expr, $fmt:expr $(, $($args:expr),*)?) => {{
        #[cfg(feature = "imm")]
        {
            log_buffer!(writeln, $buf, $fmt $(, $($args),*)?)
        }
    }};
}

/* Same as `bnop`, `bnopl`
#[macro_export]
macro_rules! bnoep {
}

#[macro_export]
macro_rules! bnoepl {
}
*/

// === Print in Non-"imm" Mode (No Buffer) ===

#[macro_export]
macro_rules! np {
    ($fmt:expr $(, $($args:expr),*)?) => {{
        f_conditional_print!(not(imm), print, $fmt $(, $($args),*)?)
    }};
}

#[macro_export]
macro_rules! npl {
    ($fmt:expr $(, $($args:expr),*)?) => {{
        f_conditional_print!(not(imm), println, $fmt $(, $($args),*)?)
    }};
}

#[macro_export]
macro_rules! nep {
    ($fmt:expr $(, $($args:expr),*)?) => {{
        f_conditional_print!(not(imm), eprint, $fmt $(, $($args),*)?)
    }};
}

#[macro_export]
macro_rules! nepl {
    ($fmt:expr $(, $($args:expr),*)?) => {{
        f_conditional_print!(not(imm), eprintln, $fmt $(, $($args),*)?)
    }};
}
