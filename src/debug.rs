// =============================================================================
// Public Macros for Debug Mode Logging
// =============================================================================

///
/// 2. `conditional_log!` chooses between two logging functions based on a cfg flag.
///    Under the condition (e.g. debug_assertions), it calls a buffer-based macro (with error handling);
///    otherwise, it calls a printing macro (which returns ()):
///
#[doc(hidden)]
#[macro_export]
macro_rules! conditional_log {
    ($cfg:meta, $buf:expr, $write_macro:ident, $print_macro:ident, $fmt:expr $(, $($args:expr),*)?) => {{
        #[cfg($cfg)]
        {
            if let Err(e) = $write_macro!($buf, $fmt $(, $($args),*)?) {
                eprintln!(concat!(stringify!($write_macro), " write_macro_error: {}"), e);
            }
        }
        #[cfg(not($cfg))]
        {
            $print_macro!($fmt $(, $($args),*)?);
        }
    }};
}

///
/// 3. `conditional_log_rev!` is like `conditional_log!` but swaps the branches:
///    under the negated condition it uses the buffering method, and under the condition it uses printing.
///    (Useful for “use buffer in release” variants.)
///
#[doc(hidden)]
#[macro_export]
macro_rules! conditional_log_rev {
    ($cfg:meta, $buf:expr, $write_macro:ident, $print_macro:ident, $fmt:expr $(, $($args:expr),*)?) => {{
        #[cfg(not($cfg))]
        {
            if let Err(e) = $write_macro!($buf, $fmt $(, $($args),*)?) {
                eprintln!(concat!(stringify!($write_macro), " write_macro_error: {}"), e);
            }
        }
        #[cfg($cfg)]
        {
            $print_macro!($fmt $(, $($args),*)?);
        }
    }};
}

///
/// 4. `conditional_print!` calls a printing macro only when a cfg condition holds.
///
#[doc(hidden)]
#[macro_export]
macro_rules! conditional_print {
    ($cfg:meta, $print_macro:ident, $fmt:expr $(, $($args:expr),*)?) => {{
        #[cfg($cfg)]
        {
            $print_macro!($fmt $(, $($args),*)?);
        }
    }};
}

// === Use Buffer in Debug Mode; Print in Release Mode ===

#[macro_export]
macro_rules! bdp {
    ($buf:expr, $fmt:expr $(, $($args:expr),*)?) => {{
        conditional_log!(debug_assertions, $buf, write, print, $fmt $(, $($args),*)?)
    }};
}

#[macro_export]
macro_rules! bdpl {
    ($buf:expr, $fmt:expr $(, $($args:expr),*)?) => {{
        conditional_log!(debug_assertions, $buf, writeln, println, $fmt $(, $($args),*)?)
    }};
}

#[macro_export]
macro_rules! bdep {
    ($buf:expr, $fmt:expr $(, $($args:expr),*)?) => {{
        conditional_log!(debug_assertions, $buf, write, eprint, $fmt $(, $($args),*)?)
    }};
}

#[macro_export]
macro_rules! bdepl {
    ($buf:expr, $fmt:expr $(, $($args:expr),*)?) => {{
        conditional_log!(debug_assertions, $buf, writeln, eprintln, $fmt $(, $($args),*)?)
    }};
}

// === Use Buffer in Release Mode; Print in Debug Mode ===

#[macro_export]
macro_rules! brp {
    ($buf:expr, $fmt:expr $(, $($args:expr),*)?) => {{
        conditional_log_rev!(debug_assertions, $buf, write, print, $fmt $(, $($args),*)?)
    }};
}

#[macro_export]
macro_rules! brpl {
    ($buf:expr, $fmt:expr $(, $($args:expr),*)?) => {{
        conditional_log_rev!(debug_assertions, $buf, writeln, println, $fmt $(, $($args),*)?)
    }};
}

#[macro_export]
macro_rules! brep {
    ($buf:expr, $fmt:expr $(, $($args:expr),*)?) => {{
        conditional_log_rev!(debug_assertions, $buf, write, eprint, $fmt $(, $($args),*)?)
    }};
}

#[macro_export]
macro_rules! brepl {
    ($buf:expr, $fmt:expr $(, $($args:expr),*)?) => {{
        conditional_log_rev!(debug_assertions, $buf, writeln, eprintln, $fmt $(, $($args),*)?)
    }};
}

// === Use Buffer in Debug Mode Only; No Output in Release ===

#[macro_export]
macro_rules! bdop {
    ($buf:expr, $fmt:expr $(, $($args:expr),*)?) => {{
        #[cfg(debug_assertions)]
        {
            log_buffer!(write, $buf, $fmt $(, $($args),*)?)
        }
    }};
}

#[macro_export]
macro_rules! bdopl {
    ($buf:expr, $fmt:expr $(, $($args:expr),*)?) => {{
        #[cfg(debug_assertions)]
        {
            log_buffer!(writeln, $buf, $fmt $(, $($args),*)?)
        }
    }};
}

/* Same as `bdop`, `bdopl`
#[macro_export]
macro_rules! bdoep {
}

#[macro_export]
macro_rules! bdoepl {
}
*/

// === Print Only in Debug Mode (No Buffer) ===

#[macro_export]
macro_rules! dp {
    ($fmt:expr $(, $($args:expr),*)?) => {{
        conditional_print!(debug_assertions, print, $fmt $(, $($args),*)?)
    }};
}

#[macro_export]
macro_rules! dpl {
    ($fmt:expr $(, $($args:expr),*)?) => {{
        conditional_print!(debug_assertions, println, $fmt $(, $($args),*)?)
    }};
}

#[macro_export]
macro_rules! dep {
    ($fmt:expr $(, $($args:expr),*)?) => {{
        conditional_print!(debug_assertions, eprint, $fmt $(, $($args),*)?)
    }};
}

#[macro_export]
macro_rules! depl {
    ($fmt:expr $(, $($args:expr),*)?) => {{
        conditional_print!(debug_assertions, eprintln, $fmt $(, $($args),*)?)
    }};
}

// === Use Buffer in Release Mode Only; No Output in Debug ===

#[macro_export]
macro_rules! brop {
    ($buf:expr, $fmt:expr $(, $($args:expr),*)?) => {{
        #[cfg(not(debug_assertions))]
        {
            log_buffer!(write, $buf, $fmt $(, $($args),*)?)
        }
    }};
}

#[macro_export]
macro_rules! bropl {
    ($buf:expr, $fmt:expr $(, $($args:expr),*)?) => {{
        #[cfg(not(debug_assertions))]
        {
            log_buffer!(writeln, $buf, $fmt $(, $($args),*)?)
        }
    }};
}

/* Same as `brop`, `bropl`
#[macro_export]
macro_rules! broep {
}

#[macro_export]
macro_rules! broepl {
}
*/

// === Print in Release Mode (No Buffer) ===

#[macro_export]
macro_rules! rp {
    ($fmt:expr $(, $($args:expr),*)?) => {{
        conditional_print!(not(debug_assertions), print, $fmt $(, $($args),*)?)
    }};
}

#[macro_export]
macro_rules! rpl {
    ($fmt:expr $(, $($args:expr),*)?) => {{
        conditional_print!(not(debug_assertions), println, $fmt $(, $($args),*)?)
    }};
}

#[macro_export]
macro_rules! rep {
    ($fmt:expr $(, $($args:expr),*)?) => {{
        conditional_print!(not(debug_assertions), eprint, $fmt $(, $($args),*)?)
    }};
}

#[macro_export]
macro_rules! repl {
    ($fmt:expr $(, $($args:expr),*)?) => {{
        conditional_print!(not(debug_assertions), eprintln, $fmt $(, $($args),*)?)
    }};
}
