pub use std::io::Write;
use std::io::{stderr, stdout, BufWriter};

pub mod debug;
pub mod imm;

pub const N_BUF: bool = true;

pub fn get_buf() -> BufWriter<std::io::StdoutLock<'static>> {
    BufWriter::new(stdout().lock())
}

pub fn get_ebuf() -> BufWriter<std::io::StderrLock<'static>> {
    BufWriter::new(stderr().lock())
}

///
/// Helper Macros (not exported):
///
/// 1. `log_buffer!` wraps a call to a buffering macro (like `write!` or `writeln!`)
///    and, if an error occurs, prints an error message.
///
#[doc(hidden)]
#[macro_export]
macro_rules! log_buffer {
    ($write_macro:ident, $buf:expr, $fmt:expr $(, $($args:expr),*)?) => {{
        if let Err(e) = $write_macro!($buf, $fmt $(, $($args),*)?) {
            eprintln!(concat!(stringify!($write_macro), " write_macro_error: {}"), e);
        }
    }};
}

// === Always Use Buffer Macros ===

#[macro_export]
macro_rules! bp {
    ($buf:expr, $fmt:expr $(, $($args:expr),*)?) => {{
        log_buffer!(write, $buf, $fmt $(, $($args),*)?)
    }};
}

#[macro_export]
macro_rules! bpl {
    ($buf:expr, $fmt:expr $(, $($args:expr),*)?) => {{
        log_buffer!(writeln, $buf, $fmt $(, $($args),*)?)
    }};
}

/* Same as `bp`,`bpl`
#[macro_export]
macro_rules! bep {
}

#[macro_export]
macro_rules! bepl {
}
*/
