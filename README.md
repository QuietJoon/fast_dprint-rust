fast_dprint
====

## Usage

### Before use

Add this snippet for each function
````rust
use fast_dprint::*;

...

fn something(..) {
    let mut _buf = get_buf!();
    let mut _e_buf = get_ebuf!();

    ...

    dpl!(
        "MSG"
    );

    bdpl!(
        _e_buf,
        "MSG {}",
        123
    );
}
````

## Legend

### prefix

#### Default (whether `--release` flag)

* Using Buffer
  * Always use buffer
  * Use buffer when *debug* mode
  * Use buffer when *release* mode
  * Never use buffer
* Print when
  * Always print
  * Print when *debug* mode
  * Print when *release* mode

* `b`: Always use buffer
* `bd`: Use buffer when *debug* mode, otherwise no buffer
* `br`: Use buffer when *release* mode, otherwise no buffer
* `bdo`: Use buffer when *debug* mode, otherwise don't print
* `d`, `do`: Use no buffer when *debug* mode, otherwise don't print
* `bro`: Use buffer when *release* mode, otherwise don't print
* `r`, `ro`: Use no buffer when *release* mode, otherwise don't print

#### **imm** mode  (whether `--imm` flag)

Mainly, **imm** flag is for independent usage from **debug** mode.
* Using Buffer
  * Always use buffer
  * Use buffer when *imm* flag
  * Use buffer when not *imm* flag
  * Never use buffer
* Print when
  * Always print
  * Print when *imm* flag
  * Print when not *imm* flag

* `b`: Always use buffer
* `bi`: Use buffer when *imm* flag, otherwise no buffer
* `bn`: Use buffer when not *imm* flag, otherwise no buffer
* `bio`: Use buffer when *imm* flag, otherwise don't print
* `i`, `io`: Use no buffer when *imm* flag, otherwise don't print
* `bno`: Use buffer when not *imm* flag, otherwise don't print
* `n`, `no`: Use no buffer when not *imm* flag, otherwise don't print

### suffix

* `p!`: `print!`
* `pl!`: `println!`
* `ep!`: `eprint!`
* `epl!`: `eprintln!`

## List

* `bp`
* `bpl`
* ~~`bep`~~
* ~~`bepl`~~
* `bdp`
* `bdpl`
* `bdep`
* `bdepl`
* `brp`
* `brpl`
* `brep`
* `brepl`
* `bdop`
* `bdopl`
* ~~`bdoep`~~
* ~~`bdoepl`~~
* `dp`
* `dpl`
* `dep`
* `depl`
* `brop`
* `bropl`
* ~~`broep`~~
* ~~`broepl`~~
* `rp`
* `rpl`
* `rep`
* `repl`
