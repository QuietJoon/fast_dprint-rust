use fast_dprint::*; // or wherever your macros are defined

#[derive(Debug)]
struct C {
    a: i32,
    b: String,
}

fn main() {
    let mut buf = get_buf();
    let mut e_buf = get_ebuf();

    let test_struct = C {
        a: 42,
        b: "Hello".into(),
    };

    // ---------------------------------------------------------
    // 1) Basic 'log_buffer!' internal usage (example only)
    //    Usually you'd use bp!/bpl! or others, but here's how to call it directly:
    // ---------------------------------------------------------
    log_buffer!(write, buf, "log_buffer(write): Hello {}\n", "World");
    log_buffer!(
        writeln,
        e_buf,
        "log_buffer(writeln): C = {:?}, a={}",
        test_struct,
        test_struct.a
    );

    // ---------------------------------------------------------
    // 2) Conditional logging (internal macros)
    //    - conditional_log!
    //    - conditional_log_rev!
    //    - conditional_print!
    //
    //    The following calls demonstrate usage in code;
    //    actual output depends on build cfg flags.
    // ---------------------------------------------------------
    conditional_log!(
        debug_assertions,
        buf,
        write,
        print,
        "conditional_log: debug? => "
    );
    conditional_log!(
        debug_assertions,
        buf,
        writeln,
        println,
        "{}",
        "Hello from conditional_log!"
    );

    conditional_log_rev!(
        debug_assertions,
        buf,
        write,
        print,
        "conditional_log_rev: release? => "
    );
    conditional_log_rev!(
        debug_assertions,
        buf,
        writeln,
        println,
        "{}",
        "Hello from conditional_log_rev!"
    );

    conditional_print!(
        debug_assertions,
        println,
        "conditional_print(debug_assertions): Only in Debug => a={}, b={}",
        test_struct.a,
        test_struct.b
    );
    conditional_print!(
        not(debug_assertions),
        println,
        "conditional_print(not(debug_assertions)): Only in Release => a={}, b={}",
        test_struct.a,
        test_struct.b
    );

    // ---------------------------------------------------------
    // 3) Always Use Buffer Macros (bp, bpl)
    // ---------------------------------------------------------
    bp!(buf, "bp!: Hello {}, struct={:?}\n", "World", test_struct);
    bpl!(buf, "bpl!: Another line => a={}", test_struct.a);

    // ---------------------------------------------------------
    // 4) Use Buffer in Debug Mode; Print in Release Mode (bdp, bdpl, bdep, bdepl)
    // ---------------------------------------------------------
    bdp!(buf, "bdp!: A = {}, B = {:?}", 1, "two");
    bdpl!(buf, "bdpl!: struct={:?}, b={}", test_struct, test_struct.b);

    bdep!(
        e_buf,
        "bdep!: Writes to stderr if debug => a={}",
        test_struct.a
    );
    bdepl!(e_buf, "bdepl!: Another line => b={}", test_struct.b);

    // ---------------------------------------------------------
    // 5) Use Buffer in Release Mode; Print in Debug Mode (brp, brpl, brep, brepl)
    // ---------------------------------------------------------
    brp!(
        buf,
        "brp!: Release => buffer, Debug => print => a={}",
        test_struct.a
    );
    brpl!(buf, "brpl!: a={}, b={}", test_struct.a, test_struct.b);

    brep!(
        e_buf,
        "brep!: Release => buffer to stderr, Debug => eprint => {}",
        "some err data"
    );
    brepl!(e_buf, "brepl!: Another line => a={}", test_struct.a);

    // ---------------------------------------------------------
    // 6) Use Buffer in Debug Mode Only (No Output in Release) => bdop, bdopl
    // ---------------------------------------------------------
    bdop!(buf, "bdop!: Hello from Debug mode, a={}", test_struct.a);
    bdopl!(buf, "bdopl!: Another line => b={}", test_struct.b);

    // ---------------------------------------------------------
    // 7) Print Only in Debug Mode (No Buffer) => dp, dpl, dep, depl
    // ---------------------------------------------------------
    dp!("dp!: Print in debug => b={}", test_struct.b);
    dpl!("dpl!: Another line => struct={:?}", test_struct);
    dep!("dep!: eprint in debug => a={}", test_struct.a);
    depl!(
        "depl!: eprintln in debug => a={}, b={}",
        test_struct.a,
        test_struct.b
    );

    // ---------------------------------------------------------
    // 8) Use Buffer in Release Mode Only (No Output in Debug) => brop, bropl
    // ---------------------------------------------------------
    brop!(buf, "brop!: Hello from Release => a={}", test_struct.a);
    bropl!(buf, "bropl!: Another line => b={}", test_struct.b);

    // ---------------------------------------------------------
    // 9) Print in Release Mode (No Buffer) => rp, rpl, rep, repl
    // ---------------------------------------------------------
    rp!("rp!: Only in Release => a={}", test_struct.a);
    rpl!("rpl!: Another line => b={}", test_struct.b);
    rep!("rep!: eprint in Release => a={}", test_struct.a);
    repl!("repl!: eprintln in Release => a={}", test_struct.a);

    // ---------------------------------------------------------
    // 10) "imm" Mode Logging
    //     (Similar to debug_assertions but uses #[cfg(imm)])
    // ---------------------------------------------------------
    // 10a) Use Buffer When in "imm" Mode; Otherwise Print => bip, bipl, biep, biepl
    bip!(
        buf,
        "bip!: Only uses buffer if 'imm' set => a={}, b={}",
        test_struct.a,
        test_struct.b
    );
    bipl!(buf, "bipl!: Another line => struct={:?}", test_struct);
    biep!(
        e_buf,
        "biep!: eprint if not imm => buffer if imm => a={}",
        test_struct.a
    );
    biepl!(e_buf, "biepl!: Another line => b={}", test_struct.b);

    // 10b) Use Buffer in Non-"imm" Mode; Print in "imm" => bnp, bnpl, bnep, bnepl
    bnp!(buf, "bnp!: Opposite of bip => struct={:?}", test_struct);
    bnpl!(buf, "bnpl!: Another line => a={}", test_struct.a);
    bnep!(e_buf, "bnep!: eprint or buffer => b={}", test_struct.b);
    bnepl!(e_buf, "bnepl!: Another line => a={}", test_struct.a);

    // 10c) Use Buffer in "imm" Mode Only; No Output in Non-"imm" => biop, biopl
    biop!(buf, "biop!: imm only => a={}", test_struct.a);
    biopl!(buf, "biopl!: imm only => b={}", test_struct.b);

    // 10d) Print Only in "imm" Mode (No Buffer) => ip, ipl, iep, iepl
    ip!("ip!: Print only in imm => a={}", test_struct.a);
    ipl!("ipl!: Another line => b={}", test_struct.b);
    iep!("iep!: eprint in imm => a={}", test_struct.a);
    iepl!("iepl!: eprintln in imm => b={}", test_struct.b);

    // 10e) Use Buffer in Non-"imm" Mode Only; No Output in "imm" => bnop, bnopl
    bnop!(buf, "bnop!: Non-imm => buffer => a={}", test_struct.a);
    bnopl!(buf, "bnopl!: Another line => b={}", test_struct.b);

    // 10f) Print in Non-"imm" Mode (No Buffer) => np, npl, nep, nepl
    np!("np!: Print if not imm => a={}", test_struct.a);
    npl!("npl!: Another line => b={}", test_struct.b);
    nep!("nep!: eprint if not imm => a={}", test_struct.a);
    nepl!("nepl!: eprintln if not imm => b={}", test_struct.b);

    // ---------------------------------------------------------
    // Done!
    // Remember, depending on your build configuration:
    //  - Some macros won't output anything at runtime.
    //  - Some macros route to stdout vs stderr.
    //  - "debug_assertions", "imm", or "not(imm)" toggles the branches.
    // ---------------------------------------------------------
}
